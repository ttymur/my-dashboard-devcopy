<?php

return [
    '/usa-proxies/'         => '/purchase/us-dedicated/',
    '/usa-sneaker-proxies/' => '/purchase/us-sneaker/',
    '/german-proxies/'      => '/purchase/de-dedicated/',
    '/brazil-proxies/'      => '/purchase/br-dedicated/',
    '/aio-bot-proxies/'     => '/purchase/us-sneaker/',

    '/purchase/us-semi-dedicated/'        => '/purchase/us-semi-3/',
    '/purchase/de-semi-dedicated/'        => '/purchase/de-semi-3/',
    '/purchase/br-semi-dedicated/'        => '/purchase/br-semi-3/',
];